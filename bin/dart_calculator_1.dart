import 'dart:io';

void main() {
  print('Enter 1st number:');
  int n1 = int.parse(stdin.readLineSync()!);

  print('Enter 2nd number:');
  int n2 = int.parse(stdin.readLineSync()!);

  print('Enter symbol[+,-,*,/,%]:');
  String sb = stdin.readLineSync() as String;

  if (sb == '+') {
    print('Answer is ${plus(n1, n2)}');
  } else if (sb == '-') {
    print('Answer is ${minus(n1, n2)}');
  } else if (sb == '*') {
    print('Answer is ${multiply(n1, n2)}');
  } else if (sb == '/') {
    print('Answer is ${divide(n1, n2)}');
  } else if (sb == '%') {
    print('Answer is ${mod(n1, n2)}');
  } 
}

int plus(n1, n2) {
  return n1 + n2;
}

int minus(n1, n2) {
  return n1 - n2;
}

int multiply(n1, n2) {
  return n1 * n2;
}

double divide(n1, n2) {
  return n1 / n2;
}

int mod(n1, n2) {
  return n1 % n2;
}
